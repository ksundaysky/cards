package game.utils;

import java.util.Collections;
import java.util.List;

/**
 * @author krzysztof.niedzielski
 */
public class Deck {

    private Shuffler shuffler;

    private List<Card> cards;

    public Deck(List<Card> cards, Shuffler shuffler) {
        this.cards = cards;
        this.shuffler=shuffler;
    }

    public void addCard(Card card){
        cards.add(card);
    }

//    TODO Marcin Ogorzalek - remove have also return type, check it
    public Card getCardFromDeck(){
        Card tempCard = cards.get(0);
    //    cards.remove(0);
        return tempCard;
    }
    public boolean removeFromDeck(){
        this.cards.remove(0);return true;
    }

    public int getSize(){
        return cards.size();
    }

    public void shuffleDeck(){
        this.shuffler.shuffle(this.cards);
    }

}
