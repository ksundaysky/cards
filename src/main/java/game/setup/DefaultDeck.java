package game.setup;

import game.utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krzysztof.niedzielski
 */
public class DefaultDeck implements IPlayingCardFactory {

    private Shuffler shuffler;

    public DefaultDeck(Shuffler shuffler) {
        this.shuffler = shuffler;
    }

    public Deck createDeck() {
        return new Deck(cardsInitializer(),shuffler);
    }

    private List<Card> cardsInitializer(){

        ArrayList<Card> cards = new ArrayList<Card>();

        for(CardColor cardColor : CardColor.values())
        {
            for (CardValue cardValue : CardValue.values())
            {
                if(cardValue != CardValue.JOCKER)
                {
                    cards.add(new Card(cardColor,cardValue));
                }
            }
        }

        cards.add(new Card(CardColor.HEARTS,CardValue.JOCKER));
        cards.add(new Card(CardColor.PIKES,CardValue.JOCKER));

        return cards;
    }
}
