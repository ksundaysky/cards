package game.utils;

/**
 * @author krzysztof.niedzielski
 */
public class Card {

    private CardColor cardColor;
    private CardValue cardValue;


    public Card(CardColor cardColor, CardValue cardValue) {
        this.cardColor = cardColor;
        this.cardValue = cardValue;
    }

    public CardValue getCardValue() {
        return cardValue;
    }
}
