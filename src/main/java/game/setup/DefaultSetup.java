package game.setup;

import game.utils.Player;
import game.utils.Table;

import java.util.ArrayList;

/**
 * @author krzysztof.niedzielski
 */
public class DefaultSetup implements ISetup {

    private IPlayingCardFactory iPlayingCardFactory;

    public DefaultSetup(IPlayingCardFactory iPlayingCardFactory) {
        this.iPlayingCardFactory = iPlayingCardFactory;
    }

    public Table setup() {

        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player("Player 1"));
        players.add(new Player("Player 2"));

        return new Table(players,this.iPlayingCardFactory.createDeck());
    }

    @Override
    public boolean firstDeal(Table table) {
        table.getInitDeck().shuffleDeck();

        int players = table.getPlayers().size();
        int cards = table.getInitDeck().getSize();

        for(int i = 0; i<cards;i++)
        {
            table.getPlayers().get(i%players).getDeck().addCard(table.getInitDeck().getCardFromDeck());
            table.getInitDeck().removeFromDeck();
        }

        return true;
    }


}
