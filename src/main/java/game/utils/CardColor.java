package game.utils;

/**
 * @author krzysztof.niedzielski
 */
public enum CardColor {
    HEARTS,
    TILES,
    CLOVER,
    PIKES
}
