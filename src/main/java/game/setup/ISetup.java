package game.setup;

import game.utils.Table;

/**
 * @author krzysztof.niedzielski
 */
public interface ISetup {
    Table setup();
    boolean firstDeal(Table table);
}
