package game.process;

public enum Comunication {
    hello("Welcome in the game!"),
    winner("We have got winner!");

    String val;

    Comunication(String val) {
        this.val = val;
    }
}
