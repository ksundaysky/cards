package game.utils;

import java.util.Collections;
import java.util.List;

public class DefaultShuffler implements Shuffler {
    @Override
    public List<Card> shuffle(List<Card> cards) {
         Collections.shuffle(cards);
         return cards;
    }
}
