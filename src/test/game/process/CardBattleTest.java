package game.process;

import game.setup.DefaultDeck;
import game.setup.DefaultSetup;
import game.utils.DefaultShuffler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CardBattleTest {

    @Test
    public void testIfTwoPlayersOnTable(){
        CardBattle cardBattle = new CardBattle(new DefaultSetup(new DefaultDeck(new DefaultShuffler())));
        assertEquals(2, cardBattle.getNumberOfPlayers());
    }

}