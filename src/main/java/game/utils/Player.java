package game.utils;

import java.util.ArrayList;

/**
 * @author krzysztof.niedzielski
 */
public class Player {

    private String identifier;
    private boolean active=true;
    private Deck deck;

    public Player(Deck deck) {
        this.deck = deck;
    }

    public Player(String identifier, Deck deck) {
        this.identifier = identifier;
        this.deck = deck;
    }

    public Player(String identifier) {
        this.identifier = identifier;
        this.deck = new Deck(new ArrayList<Card>(), new DefaultShuffler());
    }

    public Deck getDeck() {
        return deck;
    }

    public String getIdentifier() {
        return identifier;
    }

    public boolean isActive(){
        return active;
    }
    public Player getPlayer(){
        return this;
    }
    private boolean isValidPlayer(){
        return deck.getSize() >= 1;
    }
    public void updatePlayer(){
        active = isValidPlayer();
    }
}
