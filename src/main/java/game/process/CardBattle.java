package game.process;

import game.setup.ISetup;
import game.utils.Card;
import game.utils.CardValue;
import game.utils.Player;
import game.utils.Table;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author krzysztof.niedzielski
 */
public class CardBattle implements IGame {

    private Table table;
    private ISetup isetup;
    private Map<Player, Card> cardsOnTable;

    CardBattle(ISetup iSetup) {
        this.isetup = iSetup;
        table = iSetup.setup();
    }

    int getNumberOfPlayers() {
        return table.getPlayers().size();
    }

    @Override
    public void setStartUp() {
        isetup.firstDeal(table);

    }

    @Override
    public void playGame() {
        communicate(Comunication.hello);
        setStartUp();
        while (table.getNumberOfActivePlayers() > 1) {
            System.out.println("in while");
            cardsNumberCheck();
            cardsOnTable = cardsOnTable();
            CardValue max = checkForHighiestCard();
            if (checkIfIsWinner(max)) {
                addCardsToWinnersDeck(whoWins(max));
            } else {
                table.getPlayers()
                        .forEach(p -> p.getDeck().shuffleDeck());
            }
        }

        communicate(Comunication.winner);
        System.out.println("Winner is : " + table.getPlayers().get(0).getIdentifier());


    }

    private CardValue checkForHighiestCard() {

        CardValue maxVal = CardValue.TWO;

        for (Map.Entry<Player, Card> entry : cardsOnTable.entrySet()) {
            if (maxVal.val < entry.getValue().getCardValue().val) {
                maxVal = entry.getValue().getCardValue();
            }
        }

        return maxVal;
    }

    private boolean checkIfIsWinner(CardValue cardValue) {

        int occurs = 0;
        for (Map.Entry<Player, Card> entry : cardsOnTable.entrySet()) {
            if (cardValue.val == entry.getValue().getCardValue().val) {
                occurs++;
            }
        }
        return occurs == 1;
    }

    private Player whoWins(CardValue cardValue) {
        for (Map.Entry<Player, Card> entry : cardsOnTable.entrySet()) {
            if (cardValue.val == entry.getValue().getCardValue().val) {
                return entry.getKey();
            }
        }
        return null;
    }

    private void addCardsToWinnersDeck(Player player) {
        for (Map.Entry<Player, Card> entry : cardsOnTable.entrySet()) {
            player.getDeck().addCard(entry.getValue());
            entry.getKey().getDeck().removeFromDeck();
        }
    }

    private void cardsNumberCheck() {
        table.getPlayers()
                .forEach(Player::updatePlayer);
    }


    private Map<Player, Card> cardsOnTable() {
        return table.getPlayers()
                .stream()
                .filter(Player::isActive)
                .collect(
                        Collectors.toMap(Player::getPlayer, p -> p.getDeck().getCardFromDeck())
                );
    }

}

