package game.utils;

import java.util.List;

/**
 * @author krzysztof.niedzielski
 */
public class Table {

    private List<Player> players;
    private Deck initDeck;

    public Table(List<Player> players, Deck initDeck) {
        this.players = players;
        this.initDeck = initDeck;
    }

    public Deck getInitDeck() {
        return initDeck;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public long getNumberOfActivePlayers(){
        return players.stream()
                .filter(p->p.isActive())
                .count();
    }
}
