package game.setup;

import game.utils.Deck;

/**
 * @author krzysztof.niedzielski
 */
public interface IPlayingCardFactory {
    Deck createDeck();
}
