package game.process;

// TODO Wojciech Makiela - ctrl + o - cleans up your imports

import game.setup.ISetup;
import game.utils.Table;

/**
 * @author krzysztof.niedzielski
 */
public interface IGame {

    void setStartUp();
    void playGame();
    default void communicate(Comunication comunication){
        System.out.println(comunication.val);
    }

}
