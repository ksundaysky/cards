package game.process;

import game.setup.DefaultDeck;
import game.setup.DefaultSetup;
import game.utils.DefaultShuffler;

/**
 * @author krzysztof.niedzielski
 */

// TODO Marcin Ogorzalek - this app have very nice modularization
public class Main {
    public static void main(String[] args) {
        IGame game = new CardBattle(new DefaultSetup(new DefaultDeck(new DefaultShuffler())));
//        game.setStartUp();
        game.playGame();

    }
}
