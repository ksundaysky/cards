package game.utils;

/**
 * @author krzysztof.niedzielski
 */
public enum CardValue {

    TWO(2),THREE(3),FOUR(4),FIVE(5),SIX(6),SEVEN(7),EIGHT(8),NINE(9),TEN(10),JACK(11),QUEEN(12),KING(13),AS(14),JOCKER(15);
    public int val;

    CardValue(int val) {
        this.val = val;
    }
}
